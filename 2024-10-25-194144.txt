国际,#genre#
CNN,https://rakuten-cnninternational-1-gb.lg.wurl.tv/playlist.m3u8
Fox LiveNow,https://fox-foxnewsnow-vizio.amagi.tv/playlist.m3u8
AXS TV Now,https://dikcfc9915kp8.cloudfront.net/hls/1080p/playlist.m3u8
RT News,https://rt-glb.rttv.com/dvr/rtnews/playlist_4500Kb.m3u8
Al Jazeera,http://live-hls-web-aje.getaj.net/AJE/01.m3u8
Fashion TV,http://91.247.68.229:8000/play/Fashion/index.m3u8
Kids TV,https://jansonmedia-kidstv-1-us.xiaomi.wurl.tv/playlist.m3u8
Wild Earth,https://wildearth-plex.amagi.tv/masterR1080p.m3u8
Trace Sports,https://lightning-tracesport-samsungau.amagi.tv/playlist1080p.m3u8
RT Documentary,https://rt-rtd.rttv.com/live/rtdoc/playlist_4500Kb.m3u8
BBC Impossible,https://bbc-impossible-1-ca.lg.wurl.tv/playlist.m3u8
History,https://da8eq3kpws4wh.cloudfront.net/v1/manifest/3722c60a815c199d9c0ef36c5b73da68a62b09d1/cc-qwqfh4ecsmf30/7f1c6847-381e-477d-9bda-b62f74400ee0/3.m3u8
TED Conference,https://tedconferences-ted-1-us.xiaomi.wurl.tv/playlist.m3u8
Ted Talks,https://tedconferences-ted-1-eu.xiaomi.wurl.tv/playlist.m3u8
Mytime,https://appletree-mytimeau-samsung.amagi.tv/playlist.m3u8
La La Life,https://amg02051-soulpublishing-amg02051c8-xiaomi-in-398.playouts.now.amagi.tv/playlist/amg02051-admecyltd-lalalifeenglish-xiaomiin/playlist.m3u8
Tastemade,https://cdn-ue1-prod.tsv2.amagi.tv/linear/amg00047-tastemade-tmintleng-xiaomi/playlist.m3u8
New Movies,https://newidco-newmovies-1-us.lg.wurl.tv/playlist.m3u8
LoveStories,https://lovestoriestv-lovestoriestv-1-eu.xiaomi.wurl.tv/playlist.m3u8
Travel XP,https://travelxp-travelxp-1-eu.xiaomi.wurl.tv/playlist.m3u8
Museum TV,https://cdn-ue1-prod.tsv2.amagi.tv/linear/amg01492-secomsasmediart-museumtven-xiaomi/playlist.m3u8
Ondemand China,https://newidco-ondemandchina-1-us.xiaomi.wurl.tv/playlist.m3u8
Young Hollywood,https://cdn-ue1-prod.tsv2.amagi.tv/linear/amg00143-younghollywoodl-younghollywood-xiaomi/playlist.m3u8
Rakuten Viki,https://newidco-rakutenviki-2-eu.xiaomi.wurl.tv/playlist.m3u8
Global Fashion Channel,https://gfcomnimedia-globalfashionchannel-1-eu.xiaomi.wurl.tv/playlist.m3u8
Crime Time,https://bamca-crimetime-lg.amagi.tv/playlist.m3u8
